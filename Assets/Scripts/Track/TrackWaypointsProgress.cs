﻿using UnityEngine;
using System.Collections;

public class TrackWaypointsProgress : MonoBehaviour
{
    public Transform target;

    public Transform[] WaypointsLine;

    [SerializeField]private float pointToPointThreshold = 4;
    private int progressNum;
    private int curTrackId;

    void Awake()
    {
        progressNum = 0;
    }

    private void Update()
    {
        if (progressNum >= WaypointsLine.Length) return;
        target.position = WaypointsLine[progressNum].position;
        target.rotation = WaypointsLine[progressNum].rotation;
        

        Vector3 targetDelta = target.position - transform.position;
        if (targetDelta.magnitude < pointToPointThreshold)
        {
            progressNum++;
        }

        if (transform.position.y < -20f)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnDrawGizmos()
    {
        if (Application.isPlaying)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawLine(transform.position, target.position);
            Gizmos.color = Color.yellow;
            Gizmos.DrawLine(target.position, target.position + target.forward);
        }
    }

    private void OnCollisionStay(Collision col)
    {
        if (col.transform.tag != "Track" || col.gameObject.GetInstanceID() == curTrackId) return;
        progressNum = 0;
        GameObject track = col.gameObject;
        curTrackId = track.GetInstanceID();
        WaypointsLine = track.GetComponentInChildren<TrackWaypointsLine>().WaypointsList;
    }
}
