﻿using UnityEngine;
using System.Collections;

public class TrackWaypointsLine : MonoBehaviour 
{
    public Transform[] WaypointsList;

    /// <summary>
    /// Private vars:
    /// </summary>
    private int numPoints;
    private float Length;

    private void OnDrawGizmos()
    {
        DrawGizmos(false);
    }

    private void OnDrawGizmosSelected()
    {
        DrawGizmos(true);
    }

    private void DrawGizmos(bool selected)
    {
        if (WaypointsList.Length > 1)
        {
            numPoints = WaypointsList.Length;

            Gizmos.color = selected ? Color.yellow : new Color(1, 1, 0, 0.2f);
            Vector3 prev = WaypointsList[0].position;

            for (int dist = 0; dist < WaypointsList.Length - 1; dist++)
            {
                Vector3 next = WaypointsList[dist + 1].position;
                Gizmos.DrawLine(prev, next);
                prev = next;
            }
        }
    }
}
