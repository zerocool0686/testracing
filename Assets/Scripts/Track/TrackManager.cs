﻿using UnityEngine;using System.Collections;using System.Collections.Generic;using System.Linq;public class TrackManager : MonoBehaviour{    public GameObject[] trackPrefabArray;    public GameObject aiCar;    private List<GameObject> _trackObjectsArray;    void Awake()    {        _trackObjectsArray = new List<GameObject>();

        InsertNewTrackObj(GetRandomTrackPrefab(trackPrefabArray), new Vector3(0f, 0f, -20f), Quaternion.identity);        InvokeRepeating("BuildTrack", 0f, 3f);    }    protected void BuildTrack()    {        if (_trackObjectsArray.Count != 0)        {            GameObject lastTrack = _trackObjectsArray.Last();
            InsertNewTrackObj(GetRandomTrackPrefab(trackPrefabArray), lastTrack.transform.FindChild("TrackEnd").transform.position, lastTrack.transform.FindChild("TrackEnd").transform.rotation);            InsertNewAICarObj();        }    }

    protected void InsertNewTrackObj(GameObject trackObj, Vector3 deployPoint, Quaternion deployRotation)    {
        _trackObjectsArray.Add(Instantiate(trackObj, deployPoint, deployRotation) as GameObject);        /*if (_trackObjectsArray.Count > 20)        {
            GameObject oldTrack = _trackObjectsArray.First();            _trackObjectsArray.Remove(oldTrack);
            Destroy(oldTrack);
        }*/            }    protected void InsertNewAICarObj()    {
        if (_trackObjectsArray.Count > 1)
        {
            Transform lastTrackTransform = _trackObjectsArray.Last().transform.FindChild("TrackWaypoints").transform;
            Instantiate(aiCar, lastTrackTransform.position, lastTrackTransform.rotation);
        }    }    protected GameObject GetRandomTrackPrefab(GameObject[] gameObjects)    {
        return gameObjects[Random.Range(0, gameObjects.Length - 1)];    }}