﻿using UnityEngine;
using System.Collections;

namespace UnitySampleAssets.Vehicles.Car
{
    public class ImputManager : MonoBehaviour
    {
        private CarController car;


        private void Awake()
        {
            car = GetComponent<CarController>();
        }

        private void FixedUpdate()
        {
        #if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBPLAYER || UNITY_STANDALONE_WIN
            float h = Input.GetAxis("Horizontal");
            float v = Input.GetAxis("Vertical");
        #else
            float h = Input.acceleration.x;
            float v = -Input.acceleration.z;
        #endif

            car.Move(h, v);
        }
    }
}
